## Deployment Strategies
    A deployment strategy is a way to change or upgrade an application. It defines how you want to deliver your software.

- Recreate Deployment:
    It is the basic form of deployment strategy. The older version is shut down and then the newer application version is scaled up to audience
    * pros
        - It is very simple and fast
        - Can be used when you cannot afford any extra infrastructure costs.
        - You don't have to manage more than one application version in parallel
    * cons
        - It is the riskiest deployment practice.
        - Involves downtime during the update process.
        - Application is inaccessible during the down time.
- Blue-Green Deployment:
    Also know as red/black deployment. You perform 2 identical deployments of your application. The blue represents the current stable version of the application, and the green represents the new application version.
    * pros
        - Zero downtime
        - Instant rollback
    * cons
        - Increases operational overhead and cost.
- Canary Deployment:
    It is also a blue/gree strategy that is more risk-averse and a more phased approach is used. Its also called rolling update
    * pros
        - Lowest risk-prone
        - Ability to test live production traffic
        - Lets you route the live production traffic to different versions of the application without any downtime
    * cons
        - can be complex
        - slow rollout
- A/B Testing:
    A/B testing deployment strategy relies on real-world statistical data to decide on a rollout or rollback as it focuses on gauging user acceptance of new application features.
    * pros
        - Best choice when you want to make release decisions with the real-world statistical data.
    * cons
        - A/B testing involve the experimental nature of its use case which can sometimes break the application, service or user experience.